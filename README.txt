--Overview--
Language Code Alias allows you to alias the standard i18n provided language codes and use that 
alias as a token. The need for this module arose from a system design requiring a specific file 
architecture that saved files by language but that didn't use the normal language code token.

--Features--
A simple UI to easily alias any enabled languages.

--Requirements--
Token (http://drupal.org/project/token)
i18n (http://drupal.org/project/i18n)


-- Usage --
Enable language code alias at admin/build/modules/list. Now give the enable user access to the module at 
admin/user/permissions. Now you can go to admin/settings/language-code-alias and you will see all of 
the languages enabled on your site paired with fields for aliasing the language.These 
aliases will be available for use as a token on any module that utilizes tokens.